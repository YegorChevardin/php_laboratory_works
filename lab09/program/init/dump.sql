CREATE TABLE logs
(
    `id`          INT         NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `name`        VARCHAR(45) NOT NULL,
    `second_name` VARCHAR(50) NOT NULL,
    `last_name`   VARCHAR(50) NOT NULL,
    `answer_1`    INT         NOT NULL,
    `answer_2`    INT         NOT NULL
);

CREATE TABLE images
(
    `id`       INT          NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `username` VARCHAR(45)  NOT NULL,
    `path`     VARCHAR(125) NOT NULL
);