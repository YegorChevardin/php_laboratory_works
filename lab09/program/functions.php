<?php
include_once('database_actions.php');

const ERROR_MESSAGE = "<h1>Sorry, server could not proceed your request, try again later!</h1>";
const PAGES_CATALOG = "." . DIRECTORY_SEPARATOR . "pages" . DIRECTORY_SEPARATOR;
const LOGS_CATALOG = "." . DIRECTORY_SEPARATOR . "logs" . DIRECTORY_SEPARATOR;
const LOG_FILE = "logs";
const ERROR_FILE = "errors";
const PASSWORD = "testtest";
const IMAGE_CATALOG = "." . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR;
const APPLICATION_DOMAIN = "http://localhost:8888/program/program.php";

$dbConnection = null;

function main()
{
    if (empty($_SESSION["assessment"])) {
        $_SESSION["assessment"] = [];
    }

    $start = $_REQUEST['page'] ?? 1;
    showPage($start);
}

function showPage($pageNumber)
{
    try {
        switch ($pageNumber) {
            case 1:
                $pagePath = "page1.html";
                break;
            case 2:
                $pagePath = "page2.html";
                break;
            case 3:
                fetch_test_request();
                $pagePath = "page3.html";
                break;
            case 4:
                fetch_test_request();
                $pagePath = "page4.html";
                break;
            case 5:
                fetch_test_request();
                $pagePath = "page5.html";
                break;
            case 6:
                if (!validate_password()) {
                    throw new Exception("Could not validate password for showing logs");
                }
                $pagePath = "page6.php";
                break;
            case 7:
                fetch_image_uploading();
                $pagePath = "page7.html";
                break;
            case 8:
                $pagePath = "page8.php";
                break;
            default:
                $pagePath = "404.html";
        }

        if (substr($pagePath, -4) === ".php") {
            require_once(PAGES_CATALOG . $pagePath);
        } else {
            $content = file_get_contents(PAGES_CATALOG . $pagePath);
            if ($content) {
                echo $content;
            } else {
                throw new Exception(ERROR_MESSAGE);
            }
        }
    } catch (Exception $e) {
        $errorMessage = $e->getMessage();
        write_to_error_file($errorMessage);
        echo "<h1>" . $errorMessage . "</h1>";
    }
}

/**
 * @throws Exception
 */
function fetch_test_request()
{
    if (isset($_GET["action"])) {
        switch ($_GET["action"]) {
            case "typeCredentials":
                $_SESSION['assessment']['name'] = $_GET['name'];
                $_SESSION['assessment']['secondName'] = $_GET['secondName'];
                $_SESSION['assessment']['lastName'] = $_GET['lastName'];
                break;
            case "typeAnswer1":
                $_SESSION['assessment']['answer1'] = $_GET['answer1'];
                break;
            case "finishTest":
                $_SESSION['assessment']['answer2'] = $_GET['answer2'];
                if (!validate_captcha($_GET['number1'], $_GET['number2'], $_GET['result'])) {
                    throw new Exception("Cannot validate captcha, try again later. <a href='/program/program.php?page=1'>Go to home</a>");
                }
                validate_arguments($_SESSION['assessment']);
                write_to_log_file(implode(", ", $_SESSION['assessment']));

                try {
                    write_assessment_to_database(connectToTheDatabase(), $_SESSION['assessment']);
                } catch (Exception $e) {
                    $errorMessage = $e->getMessage();
                    write_to_error_file($errorMessage);
                    echo "<h1>" . $errorMessage . "</h1>";
                }

                break;
            default:
                throw new Exception("Cannot resolve the request, due to nonexistent action + {$_GET['action']}");
        }
    } else {
        throw new Exception("Cannot resolve the request, due to undefined action");
    }
}

/**
 * @throws Exception
 */
function fetch_image_uploading()
{
    if (isset($_POST['action']) && $_POST['action'] === 'uploadImage') {
        $imageFile = $_FILES['image'];

        if ($imageFile['error'] === UPLOAD_ERR_OK) {
            $allowedExtensions = ['jpg', 'jpeg', 'png', 'gif'];
            $imageExtension = strtolower(pathinfo($imageFile['name'], PATHINFO_EXTENSION));

            if (in_array($imageExtension, $allowedExtensions) && getimagesize($imageFile['tmp_name'])) {
                $uniqueFilename = uniqid() . '.' . $imageExtension;

                if (!move_uploaded_file($imageFile['tmp_name'], IMAGE_CATALOG . $uniqueFilename)) {
                    throw new Exception("Failed to move the uploaded data.");
                }

                $imageUsername = $_POST['username'];
                upload_image_info_to_database(connectToTheDatabase(), $imageUsername, $uniqueFilename);
                redirect(APPLICATION_DOMAIN);
            } else {
                throw new Exception("Invalid image format");
            }
        } else {
            throw new Exception("Cannot upload this image.");
        }

    }
}

function validate_captcha(int $firstNumber, int $secondNumber, int $resultNumber): bool
{
    return $firstNumber + $secondNumber == $resultNumber;
}

function validate_password(): bool
{
    $actualPassword = $_POST['password'];
    return $actualPassword == PASSWORD;
}

function write_to_error_file(string $content)
{
    write_to_some_file($content, LOGS_CATALOG . ERROR_FILE . ".txt");
}

function write_to_log_file(string $content)
{
    write_to_some_file($content, LOGS_CATALOG . LOG_FILE . ".txt");
}

function write_to_some_file(string $content, string $pathToFile)
{
    file_put_contents($pathToFile, $content . "\n", FILE_APPEND);
}

function read_from_log_file(): string
{
    $lines = file(LOGS_CATALOG . LOG_FILE . '.txt') ?? "No logs for now";

    $content = implode('', $lines);

    return "<pre><code>{$content}</code></pre>";
}

/**
 * @throws Exception
 */
function validate_arguments($arguments)
{
    foreach ($arguments as $value) {
        if (empty($value)) {
            throw new Exception("You did not complete test fully, please proceed to the beginning. <a href='/program/program.php?page=1'>Go home.</a>");
        }
    }
}

function redirect($redirectURL) {
    header('Location: ' . $redirectURL);
    exit();
}

/**
 * @throws Exception
 */
function show_images_by_username($username) : array {
    $result = [];

    $data = get_images_by_keyword(connectToTheDatabase(), $username);

    foreach ($data as $key => $value) {
        $result[$key] = ["fullPath" => IMAGE_CATALOG . $value['path'], "username" => $value['username']];
    }

    return $result;
}