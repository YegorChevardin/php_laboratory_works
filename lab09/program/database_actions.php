<?php
const PATH_TO_ENV = "init" . DIRECTORY_SEPARATOR . ".env";

const INSERT_INTO_LOG_FILE = "INSERT INTO logs(`name`, `second_name`, `last_name`, `answer_1`, `answer_2`) VALUES (:name, :second_name, :last_name, :answer_1, :answer_2)";

const SELECT_ASSESSMENTS_FROM_DB = "SELECT * FROM logs";

const INSERT_DATA_INTO_IMAGES = "INSERT INTO images(username, path) VALUES (:imageUsername, :imagePath)";

const SELECT_IMAGES_BY_USERNAME = "SELECT * FROM images WHERE username = :username";

/**
 * @throws Exception
 */
function readENVFile()
{
    if (file_exists(PATH_TO_ENV)) {
        $envFile = file(PATH_TO_ENV, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

        if ($envFile === false) {
            throw new Exception("Cannot read env file with the parameters to the database");
        }

        foreach ($envFile as $line) {
            list($key, $value) = explode('=', $line, 2);
            $_ENV[trim($key)] = trim($value);
        }
    } else {
        throw new Exception("Cannot connect to the database, due to no parameters specified");
    }
}

/**
 * @throws Exception
 */
function connectToTheDatabase(): PDO
{
    readENVFile();

    $databaseHost = $_ENV['DB_HOST'];
    $databaseName = $_ENV['DB_NAME'];
    $databaseUsername = $_ENV['DB_USER'];
    $databasePassword = $_ENV['DB_PASSWORD'];

    $conn = new PDO("mysql:host=$databaseHost;dbname=$databaseName", $databaseUsername, $databasePassword);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    return $conn;
}

function write_assessment_to_database(PDO $pdoObject, array $assessment_array)
{
    $prepared_statement = $pdoObject->prepare(INSERT_INTO_LOG_FILE);

    $prepared_statement->bindParam(':name', $assessment_array['name']);
    $prepared_statement->bindParam(':second_name', $assessment_array['secondName']);
    $prepared_statement->bindParam(':last_name', $assessment_array['lastName']);
    $prepared_statement->bindParam(':answer_1', $assessment_array['answer1'], PDO::PARAM_INT);
    $prepared_statement->bindParam(':answer_2', $assessment_array['answer2'], PDO::PARAM_INT);

    $prepared_statement->execute();
    while ($row = $prepared_statement->fetch(PDO::FETCH_ASSOC)) {
        echo $row . "<br/>";
    }
}

function read_assessments_from_database(PDO $pdoObject) : array {
    $statement = $pdoObject->query(SELECT_ASSESSMENTS_FROM_DB);
    return $statement->fetchAll(PDO::FETCH_ASSOC);
}

function upload_image_info_to_database(PDO $pdoObject, string $imageUsername, string $imagePath) {
    $prepared_statement = $pdoObject->prepare(INSERT_DATA_INTO_IMAGES);

    $prepared_statement->bindParam(':imageUsername', $imageUsername);
    $prepared_statement->bindParam(':imagePath', $imagePath);

    $prepared_statement->execute();
}

function get_images_by_keyword(PDO $pdoObject, string $keyword) : array {
    $prepared_statement = $pdoObject->prepare(SELECT_IMAGES_BY_USERNAME);

    $prepared_statement->bindParam(':username', $keyword);

    $prepared_statement->execute();

    return $prepared_statement->fetchAll();
}
