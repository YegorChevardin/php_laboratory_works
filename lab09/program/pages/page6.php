<?php
$logs = read_from_log_file();
$logs_from_database = false;
try {
    $array = read_assessments_from_database(connectToTheDatabase());
    foreach ($array as $key => $value) {
        $array[$key] = implode(", ", $value);
    }
    $logs_from_database = implode("</br>", $array);
} catch (Exception $e) {
    $errorMessage = $e->getMessage();
    write_to_error_file($errorMessage);
    echo "<h1>" . $errorMessage . "</h1>";
    die();
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Logs</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col-md-12">
            <div class="text-center mt-5 mb-3">
                <div class="d-flex flex-column gap-2">
                    <div>
                        <h3>Logs from the application!</h3>
                        <a href="/program/program.php?page=1">Go to home</a>
                        <div class="mt-5 p-5 rounded rounded-3 text-start bg-dark text-light shadow-lg">
                            <?php echo $logs; ?>
                        </div>
                        <br/>
                        <h1 class="mt-4">From database: </h1>
                        <div class="mt-2 p-5 rounded rounded-3 text-start bg-dark text-light shadow-lg">
                            <?php if ($logs_from_database) {
                                echo $logs_from_database;
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL"
        crossorigin="anonymous"></script>
</body>
</html>