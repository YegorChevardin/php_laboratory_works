<?php
$imageUsername = $_REQUEST['imageUsername'];
if (empty($imageUsername)) {
    redirect(APPLICATION_DOMAIN . "?page=404");
}
try {
    $imagesArray = show_images_by_username($imageUsername);
} catch (Exception $exception) {
    $errorMessage = $exception->getMessage();
    write_to_error_file($errorMessage);
    echo "<h1>" . $errorMessage . "</h1>";
    die();
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>See your images</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col-md-8">
            <div class="text-center mt-5 mb-3">
                <div class="d-flex flex-column gap-2">
                    <h1 class="mb-2">Images by <code><?php echo $imageUsername; ?></code> keyword</h1>
                    <a href="/program/program.php" class="btn btn-outline-primary mb-2">Go home</a>
                    <div class="mt-5 d-flex flex-column gap-3 justify-content-center align-items-center">
                        <?php
                        foreach ($imagesArray as $image) {
                            echo "<img class='img-fluid w-75 rounded rounded-3 shadow-lg' src='{$image['fullPath']}' alt='{$image['username']}' />";
                        }
                        ?>
                        <!-- todo show -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL"
        crossorigin="anonymous"></script>
</body>
</html>