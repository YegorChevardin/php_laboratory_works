<?php

function displayVariable($variable, $sectionName)
{
    echo "---------------" . $sectionName . "-------------------";
    echo "<pre>";
    var_dump($variable);
    echo "</pre>";
}

function printArrayInColumn($array)
{
    foreach ($array as $key => $value) {
        echo $key . ". " . $value . "<br/>";
    }
}

function findWordsWithSpecifiedLetters($text, $letterNumber, $letterValue): array
{
    $pattern = '/\b\pL+\b/u';
    preg_match_all($pattern, $text, $matches);

    $words = $matches[0];
    $result = array();

    foreach ($words as $word) {
        $normalizedWord = mb_strtolower($word, 'UTF-8');
        if (mb_strlen($normalizedWord, 'UTF-8') >= $letterNumber) {
            $letter = mb_substr($normalizedWord, $letterNumber - 1, 1, 'UTF-8');
            if ($letter === $letterValue) {
                $result[] = $word;
            }
        }
    }

    usort($result, function ($a, $b) {
        $a = mb_substr(mb_strtolower($a, 'UTF-8'), 1, 1, 'UTF-8');
        $b = mb_substr(mb_strtolower($b, 'UTF-8'), 1, 1, 'UTF-8');
        return strcmp($a, $b);
    });

    return $result;
}

function formatWordsInArray($strings): string
{
    $result = array();
    $counter = 0;

    foreach ($strings as $string) {
        $words = preg_split('/\s+/', $string, -1, PREG_SPLIT_NO_EMPTY);

        foreach ($words as $word) {
            $counter++;
            if ($counter % 6 == 0) {
                $word = mb_strtoupper($word, 'UTF-8');
            }
            $result[] = $word;
        }
    }

    return implode(', ', $result);
}

function selectWordsFromPhrase($phrase)
{
    $words = explode(', ', $phrase);

    return array_slice($words, 12, 8);
}


function getStatisticArray($array): array
{
    $statisticArray = [];
    $vowels = ['а', 'е', 'є', 'и', 'і', 'ї', 'о', 'у', 'ю', 'я'];
    $consonants = ['б', 'в', 'г', 'ґ', 'д', 'ж', 'з', 'к', 'л', 'м', 'н', 'п', 'р', 'с', 'т', 'ф', 'х', 'ц', 'ч', 'ш', 'щ'];

    foreach ($array as $value) {
        $str = mb_strtolower($value, 'UTF-8');
        $vowelCount = $consonantCount = 0;

        for ($i = 0; $i < mb_strlen($str, 'UTF-8'); $i++) {
            $char = mb_substr($str, $i, 1, 'UTF-8');
            if (in_array($char, $vowels)) {
                $vowelCount++;
            } elseif (in_array($char, $consonants)) {
                $consonantCount++;
            }
        }

        $statisticArray[$value] = [(int) round(strlen($value) / 2), $vowelCount, $consonantCount];
    }
    return $statisticArray;
}

function getStatisticFromTest($text) {
    $pattern = '/\b\pL+\b/u';
    preg_match_all($pattern, $text, $matches);
    $words = $matches[0];

    $wordsCount = count($words);
    $statisticArr = getStatisticArray($words);
    $letterCount = 0;
    $vowelCount = 0;
    $consonantCount = 0;

    foreach ($statisticArr as $value) {
        $letterCount += $value[0];
        $vowelCount += $value[1];
        $consonantCount += $value[2];
    }

    echo "Word count: {$wordsCount}" . "<br/>";
    echo "Letter count: {$letterCount}" . "<br/>";
    echo "Vowel count: {$vowelCount}" . "<br/>";
    echo "Consonant count: {$consonantCount}" . "<br/>";
}

$data = file_get_contents("./text.html");
echo $data;

$formatData = preg_replace('/<[^>]*>/', '', $data);
echo $formatData . "<br/>";

$expectedWords = findWordsWithSpecifiedLetters($formatData, 5, 'н');
$expectedWordsSize = count($expectedWords);

echo "Array size: {$expectedWordsSize}" . "<br/>";
printArrayInColumn($expectedWords);
echo "<br/>";

$wordsRaw = formatWordsInArray($expectedWords);
echo $wordsRaw . "<br/>";

$subArray = selectWordsFromPhrase($wordsRaw);
displayVariable($subArray, "ARRAY WITH NEEDED WORDS");

echo "<br/>";

$statArr = getStatisticArray($subArray);
displayVariable($statArr, "ARRAY WITH STATISTIC");

getStatisticFromTest($formatData);
